package com.chengww;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.Gson;


public class Main {
	
	public static void main(String args[]){
		System.out.println("欢迎使用智能投票系统");
		while(true){
			System.out.println("请在更换好ip后按回车键开始投票，每个ip50次");
			System.out.println("输入exit或点击右上角X退出");
			Scanner scanner = new Scanner(System.in);
			String line = scanner.nextLine();
			if(!"exit".equalsIgnoreCase(line)){
				vote();
			}
			
		}
	}
	
	public static void vote(){
		for (int i = 0; i < 50; i++) {
			System.out.println("正在第" + (i + 1) + "次投票");
			URL url;
			String code = null;
			try {
				url = new URL("http://toupiao.aldwx.com/?app_act=vote&id=233&itemid=1");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setConnectTimeout(3*1000);
				conn.setRequestMethod("GET");  
				conn.connect();
				String hint = "网络出错，请稍后重试";
				if(conn.getResponseCode() == 200){
					InputStream is = conn.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	            String strRead = null;
	            StringBuilder sbf = new StringBuilder();
	            while ((strRead = reader.readLine()) != null) {
	                sbf.append(strRead);
	                sbf.append("\r\n");
	            }
	            reader.close();
	            
	            String result = sbf.toString();

	                
	                Gson gson = new Gson();
	                Bean bean = (Bean) gson.fromJson(result,Bean.class);
	                code = bean.getCode();
	                String msg = bean.getMsg();
	                if ("0".equals(code)) hint = "投票成功，谢谢您的支持";
	                else if (msg != null && !("".equals(msg))){
	                    hint = msg;
	                }else{
	                	hint = "系统繁忙，请稍后再试";
	                }
				}
				
				System.out.println(hint);
				
				if("500".equals(code)){
					System.out.println("该ip已不可用，请切换ip后按回车键");
					break;
				}
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
		}
		
	}
}
